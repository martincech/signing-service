﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Swashbuckle.Application;

namespace pesign_api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {       
            // Attribute routes
            config.MapHttpAttributeRoutes();
            // Redirect base route url to swagger ui
            config.Routes.MapHttpRoute(
                 name: "Swagger UI",
                 routeTemplate: "",
                 defaults: null,
                 constraints: null,
                 handler: new RedirectHandler(SwaggerDocsConfig.DefaultRootUrlResolver, "swagger/ui/index"));
        }
    }
}
