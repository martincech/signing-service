﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using pesign_api.Models;
using Swashbuckle.Swagger.Annotations;
using Veit.PESign;
using NLog;
using System.Threading.Tasks;

namespace pesign_api.Controllers
{
    [Authorize]
    public class SignController : ApiController
    {
        const string CONTENT_TYPE = "application/octet-stream";

        static readonly ILogger logger = LogManager.GetCurrentClassLogger();
        static readonly object _lock = new object();

        /// <summary>
        /// Sign a binary.
        /// </summary>
        /// <remarks>Sign a binary.</remarks>
        /// <response code="200">OK</response>
        /// <response code="403">Forbidden</response>
        /// <response code="404">Internet Unavailable, Timestamp Server not Responding, Signing Token is Not Present or Sent Data is not Valid PE File.</response>
        [HttpPost]
        [Route("sign")]
        [SwaggerOperation("SignPost")]
        [SwaggerResponse(statusCode: 200, type: typeof(System.IO.Stream), description: "OK")]
        public virtual async Task<HttpResponseMessage> SignPostAsync()
        {
            if (Request.Content == null || HttpContext.Current.Request.ContentLength == 0)
            {
                logger.Warn($"Invalid request content.");
                return Request.CreateResponse(HttpStatusCode.NotFound, "Invalid request content.");
            }

            if (!string.Equals(HttpContext.Current.Request.ContentType, CONTENT_TYPE, StringComparison.OrdinalIgnoreCase))
            {
                logger.Warn($"Invalid request content type({HttpContext.Current.Request.ContentType}).");
                return Request.CreateResponse(HttpStatusCode.NotFound, "Invalid request content type.");
            }

            try
            {
                var filePath = await SaveFileAsync();
                lock (_lock)
                {
                    logger.Debug($"Sign file '{filePath}'.");
                    using (var cert = Signer.SelectLocalCert(Settings.CertThumbprint))
                    using (var hCert = new Win32.Crypt.SafeCertContextHandle(cert.Handle))
                    {
                        Signer.SignWithTimestamp(
                            filePath,
                            hCert,
                            Settings.TimestampUrl,
                            Settings.Retries,
                            Settings.Timeout
                        );
                    }
                    logger.Debug($"Create response with signed file.");
                    return CreateResponse(filePath);
                }
            }
            catch (Exception e)
            {
                logger.Error(e, "Failed to sing file.");
                return Request.CreateResponse(HttpStatusCode.NotFound, e.Message);
            }
        }

        async Task<string> SaveFileAsync()
        {
            var directory = HttpContext.Current.Server.MapPath(Settings.UploadDirectory);
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);
            var filePath = Path.Combine(directory, Guid.NewGuid().ToString());
            logger.Debug($"Save request content to file '{filePath}'.");
            using (var fileStream = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.Read, 4096, true))
            {
                await Request.Content.CopyToAsync(fileStream);
            }
            return filePath;
        }

        private static HttpResponseMessage CreateResponse(string filePath)
        {
            var result = new HttpResponseMessage(HttpStatusCode.OK);
            var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.None, 4096, FileOptions.DeleteOnClose);
            result.Content = new StreamContent(stream);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            return result;
        }
    }
}
