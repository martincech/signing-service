﻿using System;
using System.IO;
using System.Web.Http;
using System.Linq;
using NLog;
using pesign_api.Models;

namespace pesign_api
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        static readonly ILogger logger = LogManager.GetCurrentClassLogger();

        protected void Application_Start()
        {
            logger.Info("Application start.");
            GlobalConfiguration.Configure(WebApiConfig.Register);
            GlobalConfiguration.Configuration.MessageHandlers.Add(new AuthorizationHeaderHandler());
            CleanDirectory(Server.MapPath(Settings.UploadDirectory));
        }

        static void CleanDirectory(string directory)
        {
            var di = new DirectoryInfo(directory);
            if (di.Exists)
            {
                foreach (var file in di.GetFiles())
                {
                    file.Delete();
                }
            }
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            var app = (WebApiApplication)sender;
            if (app.Request.HttpMethod == "POST")
            {
                logger.Debug($"POST {app.Request.Path} request | content-length: {app.Request.ContentLength} | App memory usage: {GC.GetTotalMemory(false)}");
            }
        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            var app = (WebApiApplication)sender;
            if (app.Request.HttpMethod == "POST")
            {
                var contentLength = app.Response.Headers.AllKeys.Contains("Content-Length")
                    ? app.Response.Headers["Content-Length"]
                    : "";
                logger.Debug($"POST {app.Request.Path} response | status-code: {app.Response.Status} | content-length: {contentLength}\n");
            }
        }
    }
}
