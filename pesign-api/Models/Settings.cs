﻿using System.Configuration;

namespace pesign_api.Models
{
    public static class Settings
    {
        const string API_KEY = "API_KEY";
        const string CERT_PIN = "CERT_PIN";
        const string CERT_THUMBPRINT = "CERT_THUMBPRINT";
        const string TIMESTAMP_URL = "TIMESTAMP_URL";
        const string RETRIES = "RETRIES";
        const string TIMEOUT = "TIMEOUT";

        public const string UploadDirectory = "~/App_Data/uploads";
        public static string ApiKey => ConfigurationManager.AppSettings[API_KEY];
        public static string CertPin => ConfigurationManager.AppSettings[CERT_PIN];
        public static string CertThumbprint => ConfigurationManager.AppSettings[CERT_THUMBPRINT];
        public static string TimestampUrl => ConfigurationManager.AppSettings[TIMESTAMP_URL];
        public static int Retries
        {
            get
            {
                return int.TryParse(ConfigurationManager.AppSettings[RETRIES], out var retries) ? retries : 5;
            }
        }
        public static int Timeout
        {
            get
            {
                return int.TryParse(ConfigurationManager.AppSettings[TIMEOUT], out var timeout) ? timeout : 2000;
            }
        }
    }
}
