﻿using System;
using System.Security.Cryptography.X509Certificates;
using Veit.PESign;

namespace pesign_example
{
    class Program
    {
        static void TestEv()
        {
            const string pin = "token password";
            const string certThumb = "certificate hash";
            const string timestampUrl = "http://timestamp.comodoca.com";
            const string fileToSign = @"s:\path\to\test\file.dll";

            // unlock the token and get certificate by hash
            var hCert = Signer.SelectEvCert(pin, certThumb);

            // sign binary if we have certificate
            if (hCert != null)
                Signer.SignWithTimestamp(fileToSign, hCert, timestampUrl);
        }

        static void Test()
        {
            const string certThumb = "";
            const string timestampUrl = "http://timestamp.comodoca.com";
            const string fileToSign = @"";

            var cert = Signer.SelectLocalCert(certThumb);
            Console.WriteLine($"\t{cert.SubjectName.Name}, {cert.Thumbprint}, {cert.GetExpirationDateString()}");
            // sign binary if we have certificate
            if (cert != null)
            {
                var hCert = new Win32.Crypt.SafeCertContextHandle(cert.Handle);
                Signer.SignWithTimestamp(fileToSign, hCert, timestampUrl);
            }
        }

        static void PrintCertStore(X509Store store, string name)
        {
            Console.WriteLine($"Store: {name}");
            store.Open(OpenFlags.ReadOnly);
            foreach (var cert in store.Certificates)
            {
                Console.WriteLine($"\t{cert.SubjectName.Name}, {cert.Thumbprint}, {cert.GetExpirationDateString()}");
            }
            Console.WriteLine();
        }

        static void PrintCertStores()
        {
            using (var store = new X509Store(StoreName.My, StoreLocation.LocalMachine))
                PrintCertStore(store, "MyLocal");
            using (var store = new X509Store(StoreName.My, StoreLocation.CurrentUser))
                PrintCertStore(store, "MyCurrent");
            using (var store = new X509Store(StoreName.Root, StoreLocation.LocalMachine))
                PrintCertStore(store, "RootLocal");
            using (var store = new X509Store(StoreName.Root, StoreLocation.CurrentUser))
                PrintCertStore(store, "RootCurrent");
        }

        static void Main()
        {
            //PrintCertStores();
            //Console.WriteLine("Start debugging");
            //Console.ReadLine();
            Test();
        }
    }
}
