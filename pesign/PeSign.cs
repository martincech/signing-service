﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using NLog;
using Polly;
using Win32.Mssign;

namespace Veit.PESign
{
    public class Signer
    {
        static readonly ILogger logger = LogManager.GetCurrentClassLogger();

        const string MsEnhancedCryptProviderName = "Microsoft Enhanced Cryptographic Provider v1.0";
        const string DefaultProviderName = "eToken Base Cryptographic Provider";
        const string DefaultTokenName = "\\\\.\\AKS ifdh 0";

        static Win32.AdvApi.SafeCryptProvHandle GetCPS(uint flags = 0, string ProviderName = DefaultProviderName, string TokenName = DefaultTokenName)
        {
            if (!Win32.AdvApi.Native.CryptAcquireContext(
                out var hProv,
                TokenName,
                ProviderName,
                Win32.AdvApi.Native.PROV_RSA_FULL,
                flags
            ))
            {
                var error = Marshal.GetLastWin32Error();
                logger.Error($"Opening token {TokenName} has failed while calling CryptAcquireContext, error 0x{error:X}");
                return null;
            }
            return hProv;
        }

        static bool UnlockToken(Win32.AdvApi.SafeCryptProvHandle hProv, string TokenPin)
        {
            if (!Win32.AdvApi.Native.CryptSetProvParam(hProv, Win32.AdvApi.Native.PP_SIGNATURE_PIN, Encoding.ASCII.GetBytes(TokenPin), 0))
            {
                var Error = Marshal.GetLastWin32Error();
                logger.Error($"Failed to unlock token, error 0x{Error:X}");
                return false;
            }
            logger.Info("Successfully unlocked token.");
            return true;
        }

        static IEnumerable<Win32.Crypt.SafeCertContextHandle> GetFileSigningKeys(Win32.AdvApi.SafeCryptProvHandle hProv)
        {
            uint[] pKeySpecs = { Win32.AdvApi.Native.AT_KEYEXCHANGE, Win32.AdvApi.Native.AT_SIGNATURE };

            // Loop over all the key specs
            for (var i = 0; i < 2; i++)
            {
                if (Win32.AdvApi.Native.CryptGetUserKey(
                    hProv,
                    pKeySpecs[i],
                    out var hKey
                 ))
                {
                    uint dwCertLen = 0;
                    if (Win32.AdvApi.Native.CryptGetKeyParam(
                        hKey,
                        Win32.AdvApi.Native.KP_CERTIFICATE,
                        null,
                        ref dwCertLen,
                        0
                    ))
                    {
                        var pbCert = new byte[dwCertLen];
                        if (Win32.AdvApi.Native.CryptGetKeyParam(
                            hKey,
                            Win32.AdvApi.Native.KP_CERTIFICATE,
                            pbCert,
                            ref dwCertLen,
                            0
                        ))
                        {
                            using (var pCertContext = Win32.Crypt.Native.CertCreateCertificateContext(
                                Win32.AdvApi.Native.X509_ASN_ENCODING | Win32.AdvApi.Native.PKCS_7_ASN_ENCODING,
                                pbCert,
                                dwCertLen)
                            )
                            {
                                if (!pCertContext.IsInvalid)
                                {
                                    yield return pCertContext;
                                }
                            }
                        }
                    }
                }
            }
        }

        public static Win32.Crypt.SafeCertContextHandle SelectEvCert(string tokenPin, string certThumbprint)
        {
            using (var hProv = GetCPS(Win32.AdvApi.Native.CRYPT_SILENT))
            {
                if (UnlockToken(hProv, tokenPin))
                {
                    foreach (var hCert in GetFileSigningKeys(hProv))
                    {
                        using (var cert = new X509Certificate2(hCert.Duplicate().DangerousGetHandle()))
                        {
                            if (cert.Thumbprint == certThumbprint)
                            {
                                logger.Info($"Certificate with hash {certThumbprint} has been found.");
                                return hCert.Duplicate();
                            }
                        }
                    }
                }
                else return null;
            }
            logger.Error($"Certificate with hash {certThumbprint} has not been found.");
            return null;
        }

        public static Win32.Crypt.SafeCertContextHandle SelectCert(string certThumbprint)
        {
            using (var hProv = GetCPS(0, MsEnhancedCryptProviderName, null))
            {
                foreach (var hCert in GetFileSigningKeys(hProv))
                {
                    using (var cert = new X509Certificate2(hCert.Duplicate().DangerousGetHandle()))
                    {
                        if (cert.Thumbprint == certThumbprint)
                        {
                            logger.Info($"Certificate with hash {certThumbprint} has been found.");
                            return hCert.Duplicate();
                        }
                    }
                }
            }
            logger.Error($"Certificate with hash {certThumbprint} has not been found.");
            return null;
        }

        public static X509Certificate2 SelectLocalCert(string certThumbprint)
        {
            using (var store = new X509Store(StoreName.My, StoreLocation.LocalMachine))
            {
                store.Open(OpenFlags.ReadOnly);
                foreach (var cert in store.Certificates)
                {
                    if (cert.Thumbprint.ToUpper() == certThumbprint.ToUpper())
                    {
                        logger.Info($"Certificate found.");
                        return cert;
                    }
                }
                logger.Warn($"Certificate NOT found.");
                return null;
            }
        }

        static SafeStructureHandle<SIGNER_SUBJECT_INFO> CreateSignerSubjectInfo(string path)
        {
            var info = new SIGNER_SUBJECT_INFO
            {
                cbSize = (uint)Marshal.SizeOf(typeof(SIGNER_SUBJECT_INFO)),
                pdwIndex = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(uint)))
            };
            Marshal.StructureToPtr(0, info.pdwIndex, false);

            info.dwSubjectChoice = 0x1; // SIGNER_SUBJECT_FILE
            var assemblyFilePtr = Marshal.StringToHGlobalUni(path);

            var fileInfo = new SIGNER_FILE_INFO
            {
                cbSize = (uint)Marshal.SizeOf(typeof(SIGNER_FILE_INFO)),
                pwszFileName = assemblyFilePtr,
                hFile = IntPtr.Zero
            };

            info.Union1 = new SIGNER_SUBJECT_INFO.SubjectChoiceUnion
            {
                pSignerFileInfo = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(SIGNER_FILE_INFO)))
            };

            Marshal.StructureToPtr(fileInfo, info.Union1.pSignerFileInfo, false);

            var pSubjectInfo = Marshal.AllocHGlobal(Marshal.SizeOf(info));
            Marshal.StructureToPtr(info, pSubjectInfo, false);

            return new SafeStructureHandle<SIGNER_SUBJECT_INFO>(pSubjectInfo);
        }

        static SafeStructureHandle<SIGNER_CERT_STORE_INFO> CreateSignerCert(Win32.Crypt.SafeCertContextHandle hCert)
        {
            var signerCert = new SIGNER_CERT
            {
                cbSize = (uint)Marshal.SizeOf(typeof(SIGNER_CERT)),
                dwCertChoice = 0x2,
                Union1 = new SIGNER_CERT.SignerCertUnion
                {
                    pCertStoreInfo = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(SIGNER_CERT_STORE_INFO)))
                },
                hwnd = IntPtr.Zero
            };

            var certStoreInfo = new SIGNER_CERT_STORE_INFO
            {
                cbSize = (uint)Marshal.SizeOf(typeof(SIGNER_CERT_STORE_INFO)),
                pSigningCert = hCert.DangerousGetHandle(),
                dwCertPolicy = 0x2, // SIGNER_CERT_POLICY_CHAIN
                hCertStore = IntPtr.Zero
            };

            Marshal.StructureToPtr(certStoreInfo, signerCert.Union1.pCertStoreInfo, false);

            var pSignerCert = Marshal.AllocHGlobal(Marshal.SizeOf(signerCert));
            Marshal.StructureToPtr(signerCert, pSignerCert, false);

            return new SafeStructureHandle<SIGNER_CERT_STORE_INFO>(pSignerCert);
        }

        static SafeStructureHandle<SIGNER_SIGNATURE_INFO> CreateSignerSignatureInfo()
        {
            var signatureInfo = new SIGNER_SIGNATURE_INFO
            {
                cbSize = (uint)Marshal.SizeOf(typeof(SIGNER_SIGNATURE_INFO)),
                algidHash = Win32.Crypt.Native.CALG_SHA_256,
                dwAttrChoice = 0x0, // SIGNER_NO_ATTR
                pAttrAuthCode = IntPtr.Zero,
                psAuthenticated = IntPtr.Zero,
                psUnauthenticated = IntPtr.Zero
            };

            var pSignatureInfo = Marshal.AllocHGlobal(Marshal.SizeOf(signatureInfo));
            Marshal.StructureToPtr(signatureInfo, pSignatureInfo, false);

            return new SafeStructureHandle<SIGNER_SIGNATURE_INFO>(pSignatureInfo);
        }

#pragma warning disable CC0068 // Unused Method
        // not used at this moment, taken automatically from cert itself, useful in case of using SignerSignEx2
        static SafeStructureHandle<SIGNER_PROVIDER_INFO> GetProviderInfo(X509Certificate2 cert)
        {
            if (cert == null || !cert.HasPrivateKey)
            {
                return SafeStructureHandle<SIGNER_PROVIDER_INFO>.InvalidHandle;
            }

            var key = (ICspAsymmetricAlgorithm)cert.PrivateKey;
            const int PVK_TYPE_KEYCONTAINER = 2;

            if (key == null)
            {
                return SafeStructureHandle<SIGNER_PROVIDER_INFO>.InvalidHandle;
            }

            var providerInfo = new SIGNER_PROVIDER_INFO
            {
                cbSize = (uint)Marshal.SizeOf(typeof(SIGNER_PROVIDER_INFO)),
                pwszProviderName = Marshal.StringToHGlobalUni(key.CspKeyContainerInfo.ProviderName),
                dwProviderType = (uint)key.CspKeyContainerInfo.ProviderType,
                dwPvkChoice = PVK_TYPE_KEYCONTAINER,
                Union1 = new SIGNER_PROVIDER_INFO.SignerProviderUnion
                {
                    pwszKeyContainer = Marshal.StringToHGlobalUni(key.CspKeyContainerInfo.KeyContainerName)
                },
            };

            var pProviderInfo = Marshal.AllocHGlobal(Marshal.SizeOf(providerInfo));
            Marshal.StructureToPtr(providerInfo, pProviderInfo, false);

            return new SafeStructureHandle<SIGNER_PROVIDER_INFO>(pProviderInfo);
        }
#pragma warning restore CC0068 // Unused Method

        static void SignCode(uint dwFlags, IntPtr pSubjectInfo, IntPtr pSignerCert, IntPtr pSignatureInfo, IntPtr pProviderInfo, out SIGNER_CONTEXT signerContext)
        {

            var hResult = Native.SignerSignEx(
                dwFlags,
                pSubjectInfo,
                pSignerCert,
                pSignatureInfo,
                pProviderInfo,
                null,
                IntPtr.Zero,
                IntPtr.Zero,
                out signerContext
            );

            if (hResult < 0)
            {
                logger.Warn($"SignerSignEx returned 0x{hResult:X}");
                Marshal.ThrowExceptionForHR(hResult);
            }
        }

        static void TimeStampSignedCode(uint dwFlags, IntPtr pSubjectInfo, string timestampUrl, out SIGNER_CONTEXT signerContext)
        {
            var hResult = Native.SignerTimeStampEx(
                dwFlags,
                pSubjectInfo,
                timestampUrl,
                IntPtr.Zero,
                IntPtr.Zero,
                out signerContext
                );

            if (hResult < 0)
            {
                throw new TimeoutException($"\"{timestampUrl}\" could not be used at this time. Check the url, internet connection, and try again. Result 0x{hResult:X}.");
            }
        }

        public static void SignWithTimestamp(string appPath, Win32.Crypt.SafeCertContextHandle hCert, string timestampUrl, int retries = 5, int retryTimeout = 2000)
        {
            var pSignerCert = SafeStructureHandle<SIGNER_CERT_STORE_INFO>.InvalidHandle;
            var pSubjectInfo = SafeStructureHandle<SIGNER_SUBJECT_INFO>.InvalidHandle;
            var pSignatureInfo = SafeStructureHandle<SIGNER_SIGNATURE_INFO>.InvalidHandle;

            try
            {
                pSignerCert = CreateSignerCert(hCert);
                pSubjectInfo = CreateSignerSubjectInfo(appPath);
                pSignatureInfo = CreateSignerSignatureInfo();

                SignCode(0, pSubjectInfo.DangerousGetHandle(), pSignerCert.DangerousGetHandle(), pSignatureInfo.DangerousGetHandle(), IntPtr.Zero, out var signerContext);

                // Attempt to timestamp if we've got a timestampUrl.
                if (!string.IsNullOrEmpty(timestampUrl))
                {
                    Policy
                        .Handle<TimeoutException>()
                        .WaitAndRetry(retries, retryAttempt => TimeSpan.FromMilliseconds(retryTimeout))
                        .Execute(() => TimeStampSignedCode(0, pSubjectInfo.DangerousGetHandle(), timestampUrl, out signerContext));
                }
            }
            catch (CryptographicException ce)
            {
                logger.Error(ce, "An error occurred while attempting to load the signing certificate.");
                throw;
            }
            catch (Exception e)
            {
                logger.Error(e, "");
                throw;
            }
        }
    }
}
