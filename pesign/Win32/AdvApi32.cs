﻿using Microsoft.Win32.SafeHandles;
using System;
using System.Runtime.InteropServices;
using System.Security;

namespace Win32.AdvApi
{
    internal sealed class SafeCryptProvHandle : SafeHandleZeroOrMinusOneIsInvalid
    {
        public SafeCryptProvHandle()
            : base(true)
        {
        }

        public SafeCryptProvHandle(IntPtr handle)
            : base(true)
        {
            SetHandle(handle);
        }

        public static SafeCryptProvHandle InvalidHandle
        {
            get { return new SafeCryptProvHandle(IntPtr.Zero); }
        }

        [DllImport(Native.LibraryName, SetLastError = true)]
        [SuppressUnmanagedCodeSecurity]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool CryptReleaseContext(
            IntPtr hCryptProv,
            uint dwFlags
        );

        override protected bool ReleaseHandle()
        {
            return CryptReleaseContext(handle, 0);
        }
    }

    internal sealed class SafeUserKeyHandle : SafeHandleZeroOrMinusOneIsInvalid
    {
        public SafeUserKeyHandle()
           : base(true)
        {
        }

        public SafeUserKeyHandle(IntPtr handle)
           : base(true)
        {
            SetHandle(handle);
        }

        [DllImport(Native.LibraryName, SetLastError = true)]
        [SuppressUnmanagedCodeSecurity]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool CryptDestroyKey(
            IntPtr hKey
        );

        public static SafeUserKeyHandle InvalidHandle
        {
            get { return new SafeUserKeyHandle(IntPtr.Zero); }
        }

        protected override bool ReleaseHandle()
        {
            return CryptDestroyKey(handle);
        }
    }

    internal class Native
    {
        public const string LibraryName = "Advapi32.dll";

        public const uint PROV_RSA_FULL = 1;

        public const uint CRYPT_NEWKEYSET = 0x8;
        public const uint CRYPT_DELETEKEYSET = 0x10;
        public const uint CRYPT_MACHINE_KEYSET = 0x20;
        public const uint CRYPT_SILENT = 0x40;
        public const uint CRYPT_DEFAULT_CONTAINER_OPTIONAL = 0x80;
        public const uint CRYPT_VERIFYCONTEXT = 0xF0000000;

        public const uint PP_SIGNATURE_PIN = 33;

        public const uint AT_KEYEXCHANGE = 1;
        public const uint AT_SIGNATURE = 2;

        public const uint KP_CERTIFICATE = 26;

        public const uint CRYPT_ASN_ENCODING = 0x00000001;
        public const uint CRYPT_NDR_ENCODING = 0x00000002;

        public const uint X509_ASN_ENCODING = 0x00000001;
        public const uint X509_NDR_ENCODING = 0x00000002;

        public const uint PKCS_7_ASN_ENCODING = 0x00010000;
        public const uint PKCS_7_NDR_ENCODING = 0x00020000;

        [DllImport(LibraryName, CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool CryptAcquireContext(
            out SafeCryptProvHandle hProv,
            string pszContainer,
            string pszProvider,
            uint dwProvType,
            uint dwFlags
        );

        [DllImport(LibraryName, CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool CryptSetProvParam(
            SafeCryptProvHandle hProv,
            uint dwParam,
            [In] byte[] pbData,
            uint dwFlags
        );

        [DllImport(LibraryName, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool CryptGetUserKey(
            SafeCryptProvHandle hProv,
            uint dwKeySpec,
            out SafeUserKeyHandle hKey
        );

        [DllImport(LibraryName, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool CryptGetKeyParam(
            SafeUserKeyHandle hKey,
            uint dwParam,
            [Out] byte[] pbData,
            [In, Out] ref uint pdwDataLen,
            uint dwFlags
        );
    }
}
