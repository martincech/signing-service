﻿using Microsoft.Win32.SafeHandles;
using System;
using System.Runtime.InteropServices;
using System.Security;

namespace Win32.Crypt
{
    public sealed class SafeCertContextHandle : SafeHandleZeroOrMinusOneIsInvalid
    {
        public SafeCertContextHandle()
            : base(true)
        {
        }

        public SafeCertContextHandle(IntPtr handle)
            : base(true)
        {
            SetHandle(handle);
        }

        public static SafeCertContextHandle InvalidHandle => new SafeCertContextHandle(IntPtr.Zero);

        [SuppressUnmanagedCodeSecurity]
        [DllImport(Native.LibraryName, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool CertFreeCertificateContext([In] IntPtr pCertContext);

        [SecurityCritical]
        [DllImport(Native.LibraryName, SetLastError = true)]
        static extern SafeCertContextHandle CertDuplicateCertificateContext([In] IntPtr pCertContext);

        [SecurityCritical]
        override protected bool ReleaseHandle()
        {
            return CertFreeCertificateContext(handle);
        }

        [SecurityCritical]
        internal SafeCertContextHandle Duplicate()
        {
            if (handle == IntPtr.Zero)
                return InvalidHandle;

            return CertDuplicateCertificateContext(handle);
        }
    }

    internal class Native
    {
        public const string LibraryName = "Crypt32.dll";

        public const int KP_IV = 1;
        public const int KP_MODE = 4;
        public const int KP_MODE_BITS = 5;
        public const int KP_EFFECTIVE_KEYLEN = 19;

        public const int ALG_CLASS_SIGNATURE = (1 << 13);
        public const int ALG_CLASS_DATA_ENCRYPT = (3 << 13);
        public const int ALG_CLASS_HASH = (4 << 13);
        public const int ALG_CLASS_KEY_EXCHANGE = (5 << 13);

        public const int ALG_TYPE_DSS = (1 << 9);
        public const int ALG_TYPE_RSA = (2 << 9);
        public const int ALG_TYPE_BLOCK = (3 << 9);
        public const int ALG_TYPE_STREAM = (4 << 9);
        public const int ALG_TYPE_ANY = (0);

        public const int CALG_MD5 = (ALG_CLASS_HASH | ALG_TYPE_ANY | 3);
        public const int CALG_SHA1 = (ALG_CLASS_HASH | ALG_TYPE_ANY | 4);
        public const int CALG_SHA_256 = (ALG_CLASS_HASH | ALG_TYPE_ANY | 12);
        public const int CALG_SHA_384 = (ALG_CLASS_HASH | ALG_TYPE_ANY | 13);
        public const int CALG_SHA_512 = (ALG_CLASS_HASH | ALG_TYPE_ANY | 14);
        public const int CALG_RSA_KEYX = (ALG_CLASS_KEY_EXCHANGE | ALG_TYPE_RSA | 0);
        public const int CALG_RSA_SIGN = (ALG_CLASS_SIGNATURE | ALG_TYPE_RSA | 0);
        public const int CALG_DSS_SIGN = (ALG_CLASS_SIGNATURE | ALG_TYPE_DSS | 0);
        public const int CALG_DES = (ALG_CLASS_DATA_ENCRYPT | ALG_TYPE_BLOCK | 1);
        public const int CALG_RC2 = (ALG_CLASS_DATA_ENCRYPT | ALG_TYPE_BLOCK | 2);
        public const int CALG_3DES = (ALG_CLASS_DATA_ENCRYPT | ALG_TYPE_BLOCK | 3);
        public const int CALG_3DES_112 = (ALG_CLASS_DATA_ENCRYPT | ALG_TYPE_BLOCK | 9);
        public const int CALG_AES_128 = (ALG_CLASS_DATA_ENCRYPT | ALG_TYPE_BLOCK | 14);
        public const int CALG_AES_192 = (ALG_CLASS_DATA_ENCRYPT | ALG_TYPE_BLOCK | 15);
        public const int CALG_AES_256 = (ALG_CLASS_DATA_ENCRYPT | ALG_TYPE_BLOCK | 16);
        public const int CALG_RC4 = (ALG_CLASS_DATA_ENCRYPT | ALG_TYPE_STREAM | 1);

        [DllImport(LibraryName, SetLastError = true)]
        public static extern SafeCertContextHandle CertCreateCertificateContext(
            uint dwCertEncodingType,
            [In, Out] byte[] pbCertEncoded,
            uint cbCertEncoded
        );
    }
}
