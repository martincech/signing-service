﻿using Microsoft.Win32.SafeHandles;
using System;
using System.Runtime.InteropServices;

namespace Win32.Mssign
{
    internal sealed class SafeStructureHandle<T> : SafeHandleZeroOrMinusOneIsInvalid
        where T : struct
    {
        public SafeStructureHandle()
            : base(true)
        {
        }

        public SafeStructureHandle(IntPtr handle)
            : base(true)
        {
            SetHandle(handle);
        }

        public static SafeStructureHandle<T> InvalidHandle
        {
            get { return new SafeStructureHandle<T>(IntPtr.Zero); }
        }

        override protected bool ReleaseHandle()
        {
            Marshal.DestroyStructure(handle, typeof(T));
            return true;
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct SIGNER_SUBJECT_INFO
    {
        // The size, in bytes, of the structure.
        public uint cbSize;

        // This member is reserved. It must be set to zero.
        public IntPtr pdwIndex;

        // Specifies whether the subject is a file or a BLOB.
        public uint dwSubjectChoice;

        // SubjectChoiceUnion
        public SubjectChoiceUnion Union1;

        [StructLayout(LayoutKind.Explicit)]
        internal struct SubjectChoiceUnion
        {

            // A pointer to a SIGNER_FILE_INFO structure that specifies the file to sign.
            [FieldOffset(0)]
            public IntPtr pSignerFileInfo;

            // A pointer to a SIGNER_BLOB_INFO structure that specifies the BLOB to sign.
            [FieldOffset(0)]
            public IntPtr pSignerBlobInfo;
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    struct SIGNER_CERT
    {
        public uint cbSize;
        public uint dwCertChoice;
        public SignerCertUnion Union1;
        [StructLayout(LayoutKind.Explicit)]
        internal struct SignerCertUnion
        {
            [FieldOffset(0)]
            public IntPtr pwszSpcFile;
            [FieldOffset(0)]
            public IntPtr pCertStoreInfo;
            [FieldOffset(0)]
            public IntPtr pSpcChainInfo;
        };
        public IntPtr hwnd;
    }

    [StructLayout(LayoutKind.Sequential)]
    struct SIGNER_SIGNATURE_INFO
    {
        public uint cbSize;
        public uint algidHash; // ALG_ID
        public uint dwAttrChoice;
        public IntPtr pAttrAuthCode;
        public IntPtr psAuthenticated; // PCRYPT_ATTRIBUTES
        public IntPtr psUnauthenticated; // PCRYPT_ATTRIBUTES
    }

    [StructLayout(LayoutKind.Sequential)]
    struct SIGNER_FILE_INFO
    {
        public uint cbSize;
        public IntPtr pwszFileName;
        public IntPtr hFile;
    }

    [StructLayout(LayoutKind.Sequential)]
    struct SIGNER_CERT_STORE_INFO
    {
        public uint cbSize;
        public IntPtr pSigningCert; // CERT_CONTEXT
        public uint dwCertPolicy;
        public IntPtr hCertStore;
    }

    [StructLayout(LayoutKind.Sequential)]
    struct SIGNER_CONTEXT
    {
        public uint cbSize;
        public uint cbBlob;
        public IntPtr pbBlob;
    }

    [StructLayout(LayoutKind.Sequential)]
    struct SIGNER_PROVIDER_INFO
    {
        public uint cbSize;
        public IntPtr pwszProviderName;
        public uint dwProviderType;
        public uint dwKeySpec;
        public uint dwPvkChoice;
        public SignerProviderUnion Union1;
        [StructLayout(LayoutKind.Explicit)]
        internal struct SignerProviderUnion
        {
            [FieldOffset(0)]
            public IntPtr pwszPvkFileName;
            [FieldOffset(0)]
            public IntPtr pwszKeyContainer;
        };
    }

    internal class Native
    {
        public const string LibraryName = "Mssign32.dll";

        public const uint SPC_EXC_PE_PAGE_HASHES_FLAG = 0x10;
        public const uint SPC_INC_PE_PAGE_HASHES_FLAG = 0x100;
        public const uint SIG_APPEND = 0x1000;

        [DllImport(LibraryName, CharSet = CharSet.Unicode, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Error)]
        public static extern int SignerSignEx(
                           uint dwFlags,                   // DWORD
            [In]           IntPtr pSubjectInfo,            // SIGNER_SUBJECT_INFO
            [In]           IntPtr pSignerCert,             // SIGNER_CERT
            [In]           IntPtr pSignatureInfo,          // SIGNER_SIGNATURE_INFO
            [In][Optional] IntPtr pProviderInfo,           // SIGNER_PROVIDER_INFO
            [In][Optional] string pwszHttpTimeStamp,       // LPCWSTR
            [In][Optional] IntPtr psRequest,               // PCRYPT_ATTRIBUTES
            [In][Optional] IntPtr pSipData,                // LPVOID
            [Out]      out SIGNER_CONTEXT ppSignerContext  // SIGNER_CONTEXT
        );

        // Not used ATM
        [DllImport(LibraryName, CharSet = CharSet.Unicode, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Error)]
        public static extern int SignerSignEx2(
                           uint dwFlags,
            [In]           IntPtr pSubjectInfo,             // SIGNER_SUBJECT_INFO
            [In]           IntPtr pSignerCert,              // SIGNER_CERT
            [In]           IntPtr pSignatureInfo,           // SIGNER_SIGNATURE_INFO
            [In][Optional] IntPtr pProviderInfo,            // SIGNER_PROVIDER_INFO
            [In][Optional] uint dwTimestampFlags,           // DWORD
            [In][Optional] byte[] pszTimestampAlgorithmOid, // PCSTR
            [In][Optional] string pwszHttpTimeStamp,        // PCWSTR
            [In][Optional] IntPtr psRequest,                // PCRYPT_ATTRIBUTES
            [In][Optional] IntPtr pSipData,                 // PVOID
            [Out]      out SIGNER_CONTEXT ppSignerContext,  // SIGNER_CONTEXT
            [In][Optional] IntPtr pCryptoPolicy,            // PCERT_STRONG_SIGN_PARA
            [In][Optional] IntPtr pReserved                 // PVOID
        );

        [DllImport(LibraryName, CharSet = CharSet.Unicode, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Error)]
        public static extern int SignerTimeStampEx(
                           uint dwFlags,                   // DWORD
            [In]           IntPtr pSubjectInfo,            // SIGNER_SUBJECT_INFO
            [In]           string pwszHttpTimeStamp,       // LPCWSTR
            [In][Optional] IntPtr psRequest,               // PCRYPT_ATTRIBUTES
            [In][Optional] IntPtr pSipData,                // LPVOID
            [Out]      out SIGNER_CONTEXT ppSignerContext  // SIGNER_CONTEXT
        );
    }
}
